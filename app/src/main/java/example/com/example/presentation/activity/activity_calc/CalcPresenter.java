package example.com.example.presentation.activity.activity_calc;

import timber.log.Timber;

public class CalcPresenter implements ICalcView.Presenter {
    private ICalcView.View view;
    private final String TAG = "presenter";

    @Override
    public void onClick() {
        view.eventButton("Hello");
    }

    @Override
    public void onClick(String btn) {
        Timber.tag(TAG).e("onClick %s",btn);
        view.eventButton(btn);
    }

    @Override
    public void onStartView(ICalcView.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        if (view != null) view = null;
    }
}
