package example.com.example.presentation.fragment;


import example.com.example.Const;
import example.com.example.presentation.route.Route;

public class APresenter implements IAContract.Presenter {
    private IAContract.View view;

    public APresenter() {
    }

    @Override
    public void onStartView(IAContract.View view) {
        this.view = view;
        if (Route.getInstance().isStart()) {
            Route.getInstance().step(Const.AFRAGMENT_STEP, "Hello");
            Route.getInstance().stopStart();
        }
    }

    @Override
    public void onStopView() {
       view = null;
    }
}
