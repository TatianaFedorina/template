package example.com.example.presentation.activity.activity_intent_a;

import androidx.annotation.Nullable;
import android.os.Bundle;
import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.databinding.ActivityABinding;
import example.com.example.presentation.base.BaseActivity;
import example.com.example.presentation.base.BasePresenter;

public class AActivity extends BaseActivity<ActivityABinding>
implements IAView.View{
    private IAView.Presenter presenter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new APresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_a;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
    }

    @Override
    public void transactionActivity(Class<?> clazz, Data data) {
        super.transactionActivity(clazz,false,data);
    }
}