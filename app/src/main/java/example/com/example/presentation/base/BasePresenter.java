package example.com.example.presentation.base;

public interface BasePresenter<View> {
    void onStartView(View view);

    void onStopView();
}
