package example.com.example.presentation.fragment;

import example.com.example.presentation.base.BasePresenter;

public interface IAContract {
    interface View{

    }

    interface Presenter extends BasePresenter<View>{

    }
}
