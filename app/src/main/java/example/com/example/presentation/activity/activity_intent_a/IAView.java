package example.com.example.presentation.activity.activity_intent_a;

import example.com.example.data.Data;
import example.com.example.presentation.base.BasePresenter;

public interface IAView {
    interface View{
        void transactionActivity(Class<?> clazz, Data data);
    }

    interface Presenter extends BasePresenter<IAView.View> {
        void onClick();
    }
}
