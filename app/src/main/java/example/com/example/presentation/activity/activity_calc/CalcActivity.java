package example.com.example.presentation.activity.activity_calc;

import androidx.annotation.Nullable;
import android.os.Bundle;

import example.com.example.R;
import example.com.example.databinding.ActivityCalcBinding;
import example.com.example.presentation.base.BaseActivity;
import example.com.example.presentation.base.BasePresenter;


public class CalcActivity extends BaseActivity<ActivityCalcBinding> implements ICalcView.View{
    private ICalcView.Presenter presenter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new CalcPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_calc;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
    }

    @Override
    public void eventButton(String msg) {
        getBinding().tvExample.setText(msg);
    }

    @Override
    public void result(String res) {

    }
}