package example.com.example.presentation.route;

import example.com.example.Const;
import example.com.example.R;
import example.com.example.presentation.base.BaseActivityView;
import example.com.example.presentation.fragment.AFragment;

public class Route implements IRouter{
    private static IRouter instance;
    private BaseActivityView view;
    private boolean flag = true;


    private Route() {
    }

    public static synchronized IRouter getInstance(){
            if (instance == null){
                instance = new Route();
            }
            return instance;
    }

    @Override
    public void startView(BaseActivityView view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
    }

    @Override
    public void step(String cmd, String data) {
        switch (cmd){
            case Const.AFRAGMENT_STEP :
                view.transactionFragmentWithBackStack(AFragment.newInstance(data), R.id.content);
        }
    }

    @Override
    public void stopStart() {
         flag = false;
    }

    @Override
    public boolean isStart() {
        return flag;
    }

}
