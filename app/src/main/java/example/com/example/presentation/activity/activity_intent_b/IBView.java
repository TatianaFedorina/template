package example.com.example.presentation.activity.activity_intent_b;

import example.com.example.presentation.base.BasePresenter;

public interface IBView {
    interface View{

    }

    interface Presenter extends BasePresenter<View>{

    }
}
