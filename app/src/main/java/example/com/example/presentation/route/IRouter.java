package example.com.example.presentation.route;

import example.com.example.presentation.base.BaseActivityView;

public interface IRouter {
    void startView(BaseActivityView view);

    void stopView();

    void step(String cmd, String data);

    void stopStart();

    boolean isStart();
}
