package example.com.example.presentation.activity.activity_c;

import example.com.example.Const;
import example.com.example.presentation.route.Route;

public class CPresenter implements ICView.Presenter {
    private ICView.ActivityView view;

    @Override
    public void onStartView(ICView.ActivityView view) {
        this.view = view;
        Route.getInstance().step(Const.AFRAGMENT_STEP,"Word");
    }

    @Override
    public void onStopView() {
        if (view != null) view = null;
    }
}
